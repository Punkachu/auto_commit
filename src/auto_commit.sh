#! /bin/bash

#check the argument and throw a msg if it fails:
if [ $# -eq 0 ]; then
  echo "Usage: auto.commit [COMMIT_NAME]"
fi

#constant
scd="src/"
ext='*.[ch]'
#add all *.c and *.h files
find $scd -type f -name $ext -print | while read line; do
  #git add files
  git add "$line"
done

sleep 2

#add the commit name :
git commit -m "$1"

sleep 2

#push the commit :

git push

#display the current situation
git status
#git git show --name-only --oneline HEAD
git log --name-only
