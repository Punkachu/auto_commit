#! /bin/bash

#check the argument and throw a msg if it fails:
if [ $# -lt 2 ]; then
  echo "Usage: auto.commit [REGEXP_EXT_FILE] [COMMIT_NAME] [ [-f FILES] [-i] [-doc] [-o] [-r README_MSG] [-t TODO_MESSAGE] ]"
  echo "REGEXP FILE : Regexp for extension file to add to the repository. ex: *.[ch]"
  echo "COMMIT NAME : string to send to the 'git commit' command."
  echo "-i          : initialize the repository."
  echo "-doc        : GIT ADD AUTHORS TODO README ONLY"
  echo "-f          : GIT ADD FILE ONLY, BY GIVING THE ABSOLUTE PATH FROM CURREN DIRECTORY OF THE AUTO_COMMIT_EXT_SCRIPT"
fi

git status

#constant
login="* lugard_o"
scd="src/"
commit_msg=$2
#get the file extension to parse EX : *.[hc]
ext=$1

i=3

for i in $@; do
  #add readme authors and todo
  if [ $i = "-i" ]; then
    #create AUTHORS README AND TODO FILES
    touch AUTHORS README TODO
    #fill authors and customs todo & readme
    echo "$login" > AUTHORS
    cat -e AUTHORS
    #custom other files
    echo "    ********************************************\n
    *****************TODO LIST******************\n
    ********************************************" > TODO
    echo "    ********************************************\n
    *****************README !!******************\n
    ********************************************" > README
  fi
  if [ $i = "-doc" ]; then
    git add AUTHORS README TODO
    git commit -m commit_msg
    git push
    git status
    git log --name-only
    exit 0
  fi
  if [ $i = "-f" ]; then
    git add $i + 1
    git commit -m commit_msg
    git push
    git status
    exit 0
  fi
done

#add all *.c and *.h files
find $scd -type f -name $ext -print | while read line; do
  git add "$line"
done

sleep 2

#add the commit name :
git commit -m "$1"

sleep 2

#push the commit :

git push

#display the current situation
git status
#git git show --name-only --oneline HEAD
git log --name-only
